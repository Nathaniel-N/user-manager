const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        min: 4,
        max: 256
    },
    email: {
        type: String,
        required: true,
        min: 4,
        max: 256
    },
    passwordHashed: {
        type: String,
        required: true,
        min: 4,
        max: 1024
    },
    role: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('User', userSchema);