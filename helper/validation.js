const Joi = require('@hapi/joi');

const loginValidation = (data) => {
    const schema = Joi.object({
        username: Joi.string().min(4).regex(/^\w+$/).required(),
        password: Joi.string().min(4).required(),
    });
    return schema.validate(data);
}

const registerValidation = (data) => {
    const schema = Joi.object({
        username: Joi.string().min(4).regex(/^\w+$/).required(),
        email: Joi.string().min(4).required().email(),
        password: Joi.string().min(4).required(),
        role: Joi.string().required()
    });
    return schema.validate(data);
}

module.exports.loginValidation = loginValidation;
module.exports.registerValidation = registerValidation;
