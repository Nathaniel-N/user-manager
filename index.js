const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

const authRoute = require('./routes/auth');
const userRoute = require('./routes/user');

const app = express();

dotenv.config()

mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log('connected to database')
});

app.use(express.json())

app.use('/api/auth', authRoute);
app.use('/api/user', userRoute);

app.listen(3000, () => console.log('Server up and running'));