const router = require('express').Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { loginValidation } = require('../helper/validation');
const verify = require('../middleware/verifyToken');
const User = require('../model/User');
const RefreshToken = require('../model/RefreshToken');

router.post('/login', async (req, res) => {
    const { error } = loginValidation(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const user = await User.findOne({username: req.body.username});
    if (!user) {
        return res.status(400).send('Username does not exist');
    }

    const validPass = await bcrypt.compare(req.body.password, user.passwordHashed);
    if (!validPass) {
        return res.status(400).send('Password is incorrect');
    }

    const access_token = generateAccessToken(user);
    const refresh_token = jwt.sign({user}, process.env.REFRESH_TOKEN_SECRET);

    const token = new RefreshToken({
        token: refresh_token,
    });

    try{
        const savedToken = await token.save();
        res.header({'auth-token': access_token, 'refresh-token':refresh_token}).json({ access_token: access_token, refresh_token: refresh_token });
    } catch(err) {
        res.status(400).send(err);
    }

});

router.delete('/logout', verify, async (req, res) => {
    try {
        await RefreshToken.deleteOne({token: req.header('refresh-token')});
        res.status(204).send();
    } catch (err) {
        res.status(400).send(err);
    }

});

router.post('/token', async (req, res) => {
    const refresh_token = req.header('refresh-token');
    if (!refresh_token) {
        return res.status(401).send('Access Denied');
    }
    const tokenExist = await RefreshToken.findOne({token: refresh_token});
    if (!tokenExist) {
        return res.status(403).send('Invalid token');
    }
    const verified = jwt.verify(refresh_token, process.env.REFRESH_TOKEN_SECRET);
    const access_token = generateAccessToken(verified.user);

    res.header({'auth-token': access_token, 'refresh-token':refresh_token}).json({ access_token: access_token, refresh_token: refresh_token });
});

function generateAccessToken(user) {
    return jwt.sign({ user }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '30s' });
}

module.exports = router;