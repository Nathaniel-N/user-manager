const router = require('express').Router();
const bcrypt = require('bcryptjs');
const { registerValidation } = require('../helper/validation');
const { authGetUser, authRole } = require('../permissions/user');
const verify = require('../middleware/verifyToken');
const User = require('../model/User');

router.post('/register', verify, authRole('ADMIN'), async (req, res) => {
    const { error } = registerValidation(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const emailExist = await User.findOne({email: req.body.email});
    if (emailExist) {
        return res.status(400).send('Email already exists');
    }

    const usernameExist = await User.findOne({username: req.body.username});
    if (usernameExist) {
        return res.status(400).send('Username already exists');
    }

    const salt = await bcrypt.genSalt(10);
    const passwordHashed = await bcrypt.hash(req.body.password, salt);
    
    const user = new User({
        username: req.body.username,
        email: req.body.email,
        passwordHashed: passwordHashed,
        role: req.body.role
    });

    try{
        const savedUser = await user.save();
        res.send({ user: user._id });
    } catch(err) {
        res.status(400).send(err);
    }
});

router.get('/', verify, authRole('ADMIN'), async (req, res) => {
    const users = await User.find({});

    try{
        res.send(users);
    } catch(err) {
        res.status(500).send(err);
    }
});

router.get('/:username', verify, authGetUser, async (req, res) => {
    const user = await User.findOne({username: req.params.username});

    if (user) {
        res.json(user);
    }
    else {
        res.status(404).json({ msg: `No user with the username of ${req.params.username}` });
    }
});

router.patch('/:username', verify, authRole('ADMIN'), async (req, res) => {
    try{
        const user = await User.findOne({username: req.params.username});
        
        if (req.body.username) {
            user.username = req.body.username;
        }

        if (req.body.email) {
            user.email = req.body.email;
        }

        if (req.body.password) {
            const salt = await bcrypt.genSalt(10);
            const passwordHashed = await bcrypt.hash(req.body.password, salt);
            user.passwordHashed = passwordHashed;
        }

        if (req.body.role) {
            user.role = req.body.role;
        }

        await user.save();
        res.send(user);
    } catch (err) {
        res.status(404).json({ msg: `No user with the username of ${req.params.username}` });
    }
});

router.delete('/:username', verify, authRole('ADMIN'), async (req, res) => {
    try {
        const deleted = await User.deleteOne({username: req.params.username});
        if (!deleted.deletedCount) {
            return res.status(404).json({ msg: `No user with the username of ${req.params.username}` });
        }
        res.status(204).send();
    } catch (err) {
        res.status(400).send(err);
    }
});

module.exports = router;