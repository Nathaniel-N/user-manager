function authGetUser(req, res, next) {
    if (!canViewUser(req.user, req.params.username)) {
        return res.status(401).send('Not authorized');
    }

    next();
}

function authRole(role) {
    return (req, res, next) => {
        if (req.user.role !== role) {
            return res.status(401).send('Not authorized');
        }

        next();
    }
}


function canViewUser(user, username) {
    return (user.role === 'ADMIN' || user.username === username);
}

module.exports.authGetUser = authGetUser;
module.exports.authRole = authRole;