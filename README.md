# User Manager

A user manager application using NodeJS with the ExpressJS framework with MongoDB to serve as the database.

## Setup

1. Clone the repository from the command line with
```
git clone https://gitlab.com/Nathaniel-N/user-manager
```

2. Install dependencies inside the root repository

```
npm install
```

3. Run the application server

```
npm start
```

## User credentials

ADMIN
username: nathan01
password: gmestonks

USER
username: jotaro_kujo
password: starplatinum

username: jolyne_cujoh
password: stoneocean

username: jonathan_joestar
password: hamon

## API Documentation
https://documenter.getpostman.com/view/6780506/TWDRsKtG

## Flowchart
# ![application](flowchart.png)